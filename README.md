Version 1.0 - January 05 2015

Sample Wordpress plugin which loads jQuery Colorbox scripts then to demonstrate OOP skills.

Usage

1) Copy archive and unpack or git clone to wp-content/plugins folder

2) Activate plugin in wp-admin dashboard (plugin settings)

3) Wrap images in the post the following way: [test_shortcode]... code with anchors and image.. [/test_shortcode]. Make sure that anchors are linked to images

4) Set interval if default value 980 does not suit you. Also you can use shortcode attribute:
     [test_shortcode interval=somevalue]

Todo:

(possible features)

1) add width, height, transition settings, autoopen/autoclose checkboxes

2) Could be added to gallery settings

3) Some plugin settings page

4) CDN support

<?php
/*
Plugin Name: Show images in Colorbox
Plugin URI:
Description: WordPress plugin which allows to load images in Colorbox from post or page
Author: Nikita Menshutin
Version: 1.0
Author URI: http://somepage.com
*/
defined('ABSPATH') or die("No script kiddies please!");
if (!defined('ABSPATH')) {
  die("No script kiddies please!");
}
if (!class_exists('test_colorbox')){
  class test_colorbox { // Here is our class
    public $add_js_to_footer=false; // Should js scripts be enqued to footer? Set to false if no
    public $prefix='my_plugin_prefix'; // Prefix is used to prevent possible collisions (like double id)
    public $field; // field name is used as metabox id, field name and meta key
    public $interval=980; // default interval if nothing is set in post

    public function __construct(){
      add_action('init',array($this,'init'));
    }
    public function init(){
      $this->field=$this->prefix.'_colorbox_interval'; // let's assign field name
      add_action('wp_enqueue_scripts',array($this,'enqueue_colorbox')); // Add js scripts to wordpress
      add_shortcode('test_shortcode',array($this,'test_shortcode')); // we add shortcode to wrap images for our js
      add_action('add_meta_boxes',array($this,'add_meta_box')); // create metabox for setting interval value
      add_action( 'save_post',array($this,'save_post') ); // when post or page is saved,
    }
    public function enqueue_colorbox(){ // this method enqueues our scripts to wp, scripts depend on jquery, won't work if jquery is dequeued
      wp_register_script(
      'jquery-colorbox',
      plugin_dir_url( __FILE__ ) .'js/jquery.colorbox-min.js',
      array( 'jquery' ),
      '1.6.3',
      $this->add_js_to_footer
    );
    wp_register_script('jquery-colorbox-test',
    plugin_dir_url( __FILE__ ) .'js/test_plugin.js',
    array('jquery-colorbox'),
    '1.0',
    $this->add_js_to_footer
  );
  wp_enqueue_script('jquery-colorbox');
  wp_enqueue_script('jquery-colorbox-test');
}
public function test_shortcode($atts,$content){ //shortcode filter
  if(isset($atts['interval']) && is_numeric($atts['interval'])){
    // if has interval attribute and it is valid number,will use it
    $interval=$atts['interval'];
  } else {
    // else will use which is set in post
    $interval=$this->get_interval_value();
  }
  return '<span data-interval="'.(int)$interval.'" class="test_images_wrap">'.do_shortcode($content).'</span>';
}
public function add_meta_box(){ /// creating meta box with callback
  $screens = array( 'post', 'page' );
  foreach ( $screens as $screen ) {
    add_meta_box(
    $this->field,
    __( 'Colorbox interval', $this->prefix.'textdomain' ),
    array($this,'metabox_callback'),
    $screen
  );
}
}
public function get_interval_value($post_id=false){ // method to get interval value set in meta box or plugin default
  if(!$post_id){ // if post id is not passed we get it from environment
    global $post;
    $post_id=$post->ID;
  }
  $value=get_post_meta( $post_id, $this->field, true ); // getting value from post meta
  if(0===strlen($value) || 1>(int)$value){
    // if it is zero or not set we get plugin default value
    $value=$this->interval;
  }
  return $value;
}
public function metabox_callback($post){ // meta box callback method
  wp_nonce_field( $this->prefix.'_save_meta_box_data', $this->prefix.'_meta_box_nonce' );
  $value = $this->get_interval_value($post->ID);
  echo '<label for="'.$this->field.'">';
  _e( 'Enter colorbox interval (seconds)', $this->prefix.'textdomain' );
  echo '</label> ';
  echo '<input type="number" id="'.$this->field.'" name="'.$this->field.'" value="' . esc_attr( $value ) . '" size="25" />';
}

public function save_post($post_id){ //method called during post publish or update
  // Check if our nonce is set.
  if ( ! isset( $_POST[$this->prefix.'_meta_box_nonce'] ) ){
    return;
  }
  // Verify that the nonce is valid.
  if ( ! wp_verify_nonce( $_POST[$this->prefix.'_meta_box_nonce'] , $this->prefix.'_save_meta_box_data' ) ) {
    return;
  }
  // If this is an autosave, our form has not been submitted, so we don't want to do anything.
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }
  // Check the user's permissions.
  if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
    if ( ! current_user_can( 'edit_page', $post_id ) ) {
      return;
    }
  } else {
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
      return;
    }
  }
  // Make sure that it is set.
  if ( ! isset( $_POST[$this->field] ) ) {
    return;
  }
  // Make sure that value is numeric
  if(!is_numeric($_POST[$this->field])){
    return;
  }
  update_post_meta( $post_id, $this->field, $_POST[$this->field] );

}
}
$my_test_plugin=new test_colorbox(); // Declare new class;
}

jQuery(window).load(function(){$params={open:true,  width:'50%',loop: false, rel:'test_group'}; // default params
$colorbox_images='.test_images_wrap:first'; // here we get the first group of images to show in color box, all the next will be ignored
jQuery($colorbox_images+' a').colorbox($params); //bind color box to anchors
$interval=jQuery($colorbox_images).data('interval'); // getting the interval
total=jQuery($colorbox_images+' a.cboxElement').length; // total elements to show
jQuery(document).bind('cbox_complete', function(){  // on image load complete in colorbox we either skip to next in $inteval seconds or close the colorbox
  if(!is_colorbox_last_element()){
    setTimeout(jQuery.colorbox.next, $interval);
  } else {
    setTimeout(jQuery.colorbox.close, $interval)
  }
});});
function is_colorbox_last_element(){
  // this function checks if current element in color box is the last one or not
  if(total==1){
    return true;
  }
  index=jQuery($colorbox_images+' a.cboxElement').index(jQuery.colorbox.element())+1;
  if(index==total){
    return true;
  }
  return false;
}
